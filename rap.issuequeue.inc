<?php
//$Id$

/*
 * @file
 * Report a problem issue queue
 */

function rap_ticketqueue_open(){
	if(user_access('view outstanding problems')){
	$build = views_embed_view('list_tickets', 'open_tickets');
	return $build;
	}
}

function rap_ticketqueue_closed(){
	$build = views_embed_view('list_tickets', 'closed_tickets');
	return $build;
}

function rap_myticketqueue_open(){
	$build = views_embed_view('list_tickets', 'my_open_tickets');
	return $build;
}

function rap_myticketqueue_closed(){
	$build = views_embed_view('list_tickets', 'my_closed_tickets');
	return $build;
}
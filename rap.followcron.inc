<?php
 /*
  * @file
  * check for items that need attention and email the responsible party.
  */
  @require_once(drupal_get_path('module', 'rap') . '/rap.email.inc');
  
  function rap_cron(){
  	//pull created dates from the ticket database
  	$outstanding = db_query("
  	SELECT tid, created FROM {rap_ticket} 
  	WHERE closed = :closed
  	", array(':closed' => 0));
	foreach ($outstanding as $row){
		$late[] = $row;
	}
  	//print_r($late);

	$build = null;

	$rounded_diff = null;
	
	if (empty($late)){
		$build = "no problems outstanding";
		return $build;
	}else{
			for ($i=0; $i < count($late) ; $i++) { 
		 
			$created = $late[$i]->created;
			$tid = $late[$i]->tid;
			
			$num_of_days = format_interval(time() - $created, 1); //how long over due in days 
			$converted_nod = strtotime( "- 1 days"); //amount of time until email is sent at certain levels
			
			//NEED TO RESET THE TIME ONCE FOLLOW UP HAS BEEN COMPLETED.
			
			if ($created < time()){
					
				switch (TRUE) {
					case ($created < strtotime( "- 1 days")):
						$build .= 'num of days: ' .$num_of_days;
						$build .= "<br />";
						
						$build .= 'Issued has been around longer than 1 day.';
						
						$a = new EmailAlert;
						$build .= "<br />";
						$build .= $a->send_reminder($created, $tid);

					break;
				
				default:
					$build .= 'case should not be triggered';
					break;
			}
		$build .= "<br />";
		}
			
			} 
			
		$build .= "<br />";
		$build .= 'Num in array ' .count($late);
		$build .= "<br />";

		

		$build .= "<br />";
 		return $build;
 		}
	
 }

function rap_cron_check($tid){
  	//pull created dates from the ticket database
  	$late = db_query("
  	SELECT * FROM {rap_ticket} 
  	WHERE tid = :tid
  	", array(':tid' => $tid))->fetchAssoc();
	
  	//print_r($late);

	$build = null;

	$rounded_diff = null;
	
	$created = $late['created'];
	$tid = $late['tid'];
			
	$num_of_days = format_interval(time() - $created, 1); //how long over due in days 
	$converted_nod = strtotime( "- 1 days"); //amount of time until email is sent at certain levels
			
			//NEED TO RESET THE TIME ONCE FOLLOW UP HAS BEEN COMPLETED.
			
			if ($created < time()){
					
				switch (TRUE) {
					case ($created < strtotime( "- 1 days")):
						$build .= 'num of days: ' .$num_of_days;
						$build .= "<br />";
						
						$build .= 'Issued has been around longer than 1 day.';
						
						$a = new EmailAlert;
						$build .= "<br />";
						$build .= $a->send_reminder($created, $tid);

					break;
				
				default:
					$build .= 'case should not be triggered';
					break;
			}
		$build .= "<br />";
		}
			
			 
			
		$build .= "<br />";
		$build .= 'Num in array ' .count($late);
		$build .= "<br />";

		

		$build .= "<br />";
 		return $build;
 		
	
 }


 function rap_ticket_track($tid){
 	$ticketinfo = db_query("
  	SELECT * FROM {rap_ticket} 
  	WHERE tid = :tid
  	", array(':tid' => $tid));
	foreach ($ticketinfo as $row){
		$tinfo[] = $row;
	}
	
	$followupinfo = db_query("
  	SELECT * FROM {rap_followup} 
  	WHERE tid = :tid
  	", array(':tid' => $tid));
	
	foreach ($followupinfo as $row){
		$finfo[] = $row;
	}
	
	$build = null;
	$created = format_date($tinfo[0]->created, 'ticket_time');
	$tid = $tinfo[0]->tid;
	$status = $tinfo[0]->closed;
	
	if (empty($finfo)){
		$build = 'No follow-up done. <br />';
		$build .= $created . ' <br />' . $tinfo[0]->description . '<br />';
		$build .= t('<a href="../../rap/fixproblem/!tid">Follow up or close issue</a>',
				array('!tid' => $tid));
		return $build;
	}else{
			
			$numoffu = count($finfo);
			
			
			if ($status == 1){
				$status = 'CLOSED';
			} else {
				$status = 'STILL OPEN <br />';
				$status .= t('<a href="../../rap/fixproblem/!tid">Follow up or close issue</a>',
				array('!tid' => $tid));
			}
			
			$build .= 'Ticket was followed up: ' .$numoffu . 'times.<br /> Ticket was created ' . $created .'<br />';
			$build .= 'Ticket is: ' . $status . '<br />';
			
			for ($i=0; $i < count($finfo) ; $i++) { 
		 	$comment = $finfo[$i]->comment;
			$futime = $finfo[$i]->created;
			$build .= '<br />The following comments <br/>' . $comment . ' were made on ' . format_date($futime, 'ticket_time') . '<br />';
			
			}
			return $build;
 }
}
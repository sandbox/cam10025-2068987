<?php

/**
 * @file
 * Providing extra functionality for the Report a Problem module.
 */
 
 /**
  * implements hook_views_data().
  */
function rap_views_data(){
	
	$data['rap_ticket']['table']['group'] = t('Tickets');
	
	  // Advertise this table as a possible base table
  $data['rap_ticket']['table']['base'] = array(
    'field' => 'tid',
    'title' => t('Ticket'),
    'help' => t('Tickets from Report a problem.'),
    'weight' => -10,
  );
 $data['rap_tickets']['table']['join'] = array(
  	'users' => array(
		'left_field' => 'uid',
		'field' => 'uid',
	),
  );

  
  // tid
  $data['rap_ticket']['tid'] = array(
    'title' => t('tid'),
    'help' => t('The Ticket ID.'), // The help that appears on the UI,
    // Information for displaying the lid
    'field' => array(
      'handler' => 'views_handler_field', // @@@
      'click sortable' => TRUE,
    ),
    // Information for accepting a lid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    // Information for sorting on a lid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['rap_ticket']['pid'] = array(
    'title' => t('pid'),
    'help' => t('The Ticket Problem ID.'), // The help that appears on the UI,
    // Information for displaying the lid
    'field' => array(
      'handler' => 'views_handler_field', // @@@
      'click sortable' => TRUE,
    ),
    // Information for accepting a lid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    // Information for sorting on a lid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  $data['rap_ticket']['title'] = array(
    'title' => t('Title'),
    'help' => t('The name of the selected ticket.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['rap_ticket']['created'] = array(
    'title' => t('Created'),
    'help' => t('The time the ticket was created of the selected ticket.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
    $data['rap_ticket']['closed'] = array(
    'title' => t('Closed'),
    'help' => t('Is the issue still open.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    
  );
    $data['rap_ticket']['uid'] = array(
    'title' => t('uid'),
    'help' => t('The Ticket User ID.'), // The help that appears on the UI,
    // Information for displaying the lid
    'field' => array(
      'handler' => 'views_handler_field', // @@@
      'click sortable' => TRUE,
    ),
    // Information for accepting a lid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    // Information for sorting on a lid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  ); 
  $data['rap_ticket']['updated_last'] = array(
    'title' => t('Updated'),
    'help' => t('Has there been any contact.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['rap_ticket']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('Users ID for relationship.'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'title' => t('Adding the userid relationship'),
      'label' => t('User ID'),
    ),
  );
  return $data;
}
<?php

/*
 * @file
 * Report a problem map of problems clustered
 */
 
 /**
 * Implements hook_views_api().
 */
function rap_views_api() {
  return array(
    'api' => 3.0,
    'path' => drupal_get_path('module', 'rap'),
  );
}
/**
 * Implements hook_views_default_views().
 */
function rap_views_default_views() {
  return rap_all_locations();
}
 
function rap_all_locations(){
//$locations = db_query('SELECT locationID FROM {rap_ticket}') -> fetchAllKeyed();
/*	
$view = new view();
$view->name = 'ticket_locations';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'location';
$view->human_name = 'Ticket Locations';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master 
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Ticket Locations';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'gmap';
$handler->display->display_options['style_options']['markertype'] = 'cluster';
$handler->display->display_options['style_options']['latfield'] = 'lid';
$handler->display->display_options['style_options']['lonfield'] = 'lid';
$handler->display->display_options['style_options']['markerfield'] = 'lid';
$handler->display->display_options['style_options']['enablermt'] = 0;
$handler->display->display_options['style_options']['rmtfield'] = 'lid';
$handler->display->display_options['style_options']['animation'] = '0';
$handler->display->display_options['style_options']['tooltipenabled'] = 1;
$handler->display->display_options['style_options']['tooltipfield'] = 'lid';
$handler->display->display_options['style_options']['bubbletextenabled'] = 1;
$handler->display->display_options['style_options']['bubbletextfield'] = 'lid';
$handler->display->display_options['row_plugin'] = 'fields';*/
/* Field: Location: Lid 
$handler->display->display_options['fields']['lid']['id'] = 'lid';
$handler->display->display_options['fields']['lid']['table'] = 'location';
$handler->display->display_options['fields']['lid']['field'] = 'lid';
*/
/* Display: Page 
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'ticket-locations';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'All Ticket Locations';
$handler->display->display_options['menu']['name'] = 'main-menu';*/	

$view = new view();
$view->name = 'list_locations';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'location';
$view->human_name = 'List Locations';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'List Locations';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Location: Lid */
$handler->display->display_options['fields']['lid']['id'] = 'lid';
$handler->display->display_options['fields']['lid']['table'] = 'location';
$handler->display->display_options['fields']['lid']['field'] = 'lid';
/* Field: Location: Street */
$handler->display->display_options['fields']['street']['id'] = 'street';
$handler->display->display_options['fields']['street']['table'] = 'location';
$handler->display->display_options['fields']['street']['field'] = 'street';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'list-locations';

$views($view->name) = $view;
return $views;
}

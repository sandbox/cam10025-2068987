<?php
//$Id$

/*
 * @file
 * Report a problem ticket form
 */

@require_once(drupal_get_path('module', 'location') . '/supported/location.us.inc');

function rap_tickets_form(){
  $user = $GLOBALS['user'];
  $form = array();
  $node = new stdClass();
  $node->type = 'location';
  
  $form['overview'] = array(
    '#markup' => t('Report problems to city management'),
  );
  
  $options = db_query('SELECT pid, pname FROM {rap_problems}')->fetchAllKeyed();
  
  $form['problem'] = array(
    '#title' => t('Problem'),
    '#description' => t('Please select a problem from the drop down menu'),
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
  );
  $form['title'] = array(
    '#title' => t('Issue Name'),
    '#description' => t('Please give a name to your problem.'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#description' => t('Please provide a brief description of the problem.'),
    '#type' => 'textarea',
    '#required' => TRUE,
  );
  $form['contact_information'] = array(
    '#title' => t('Contact Information'),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
  );
  $form['contact_information']['ticket_fn'] = array(
	'#title' => t('First Name'),
	'#type' => 'textfield',
	'#required' => TRUE,
	'#default_value' => (isset($user->name)) ? $user->name : '',
  );
  $form['contact_information']['ticket_ln'] = array(
	'#title' => t('Last Name'),
	'#type' => 'textfield',
	'#required' => TRUE,
  );
  $form['contact_information']['ticket_email'] = array(
	'#title' => t('Contact Email'),
	'#description' => t('Email Address for follow up on submitted tickets.'),
	'#type' => 'textfield',
	'#required' => FALSE,
  );
  $form['contact_information']['ticket_phone'] = array(
	'#title' => t('Contact Phone Number'),
	'#description' => t('Phone number for follow up on submitted tickets. Please supply if email address is not submitted'),
	'#type' => 'textfield',
	'#required' => FALSE,
  );
  $form['contact_information']['street'] = array(
    '#title' => t('Street'),
    '#description' => t('If the problem is not at or in your home, please supply the house or nearest house number for the issue'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['contact_information']['additional'] = array(
    '#title' => t('Additional'),
    '#description' => t('Street address line 2.'),
    '#type' => 'textfield',
    '#required' => FALSE,
  );
  $form['contact_information']['city'] = array(
    '#title' => t('City'),
    '#type' => 'textfield',
    '#default_value' => t('Sun Prairie'),
    '#required' => TRUE,
  );
  $form['contact_information']['state'] = array(
    '#title' => t('State'),
    '#type' => 'select',
    '#options' => location_province_list_us(),
    '#default_value' => t('WI'),
    '#required' => TRUE,
  );
  $form['contact_information']['postal_code'] = array(
	'#type' => 'textfield',
    '#title' => t('Postal code'),
    '#default_value' => t('53590'),
    '#size' => 16,
  );
  $form['contact_information']['country'] = array(
    '#type' => 'hidden',
    '#default_value' => t('us'),
    '#required' => FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
	

  return $form;
}

function rap_tickets_form_submit($form, &$form_state){
	$account = $GLOBALS['user'];
	print_r($account); 
	
  	$street = array(
  		'street' => $form_state['values']['street'],
  		'additional' => $form_state['values']['additional'],
  		'province' => $form_state['values']['state'],
  		'city' => $form_state['values']['city'],

  		'country' => $form_state['values']['country'],
		'is_primary' => 1,
		);
   	
		
	  $locationID = location_save($street);
	
	  	 
	  $tid = db_insert("rap_ticket")
      ->fields(array(
        'created' => REQUEST_TIME,
        'pid' => $form_state['values']['problem'],
        'title' => $form_state['values']['title'],
        'description' => $form_state['values']['description'],
        'ticket_fn' => $form_state['values']['ticket_fn'],
        'ticket_ln' => $form_state['values']['ticket_ln'],
        'ticket_email' => $form_state['values']['ticket_email'],
        'ticket_phone' => $form_state['values']['ticket_phone'],
        'locationID' => $locationID,
        'uid' => $account->uid,
      ))
      ->execute();
	  	$form_state['redirect'] = 'rap/ticket/' .$tid;
}

function rap_fixaproblem_form($form, &$form_state, $tid) {

	
	$form = array();
	if(empty($tid))
   	throw new Exception("No Ticket ID Provided"); 
	
	$tinfo = db_query("SELECT * FROM {rap_ticket} WHERE tid = :tid", array(':tid' => $tid));
	
	foreach ($tinfo as $row){
		$info[]=$row;
	}
	
	$title = $info[0]->title;
	$created = $info[0]->created;
	
	$form['paragraph'] = array(
    '#type' => 'markup',
    '#markup' => t('Ticket ' . $title . 'submitted ' . format_date($created, 'ticket_time') .
	'<br /> Problem is ' . $info[0]->description . '<br />' .
	'Complaintants Name: ' . $info[0]->ticket_fn . ' ' . $info[0]->ticket_ln . '<br />' .
	'Complaint is at: ' . '<br /><br />'), ///STILL NEED TO PULL LOCATION INFORMATION
	);
	
	
	///ADD COMMENT/REPLY SECTION
	$form['response_form'] = array(
    '#markup' => t('<b>Ticket Resolution Communication Form</b>'),
  	);
  	$form['id'] = array(
  	'#type' => 'hidden',
  	'#value'=> $tid,
	);
	
  	$form['comment'] = array(
    '#title' => t('Response'),
    '#description' => t('Please respond with a follow up to the stated problem.'),
    '#type' => 'textarea',
    '#default value'=> '',
    '#required' => TRUE,
  	);
	$form['followup'] = array(
    '#title' => t('Closed'),
    '#description' => t('Is the problem resolved?'),
    '#type' => 'checkbox',
    '#required' => FALSE,
  	);
	
	$form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Respond'),
    '#submit' => array('rap_ticket_response_email'),
  	);
	
	return $form;
}


function rap_ticket_response_email($form, &$form_state){
	//first instert the comment information into the database
	$fid = db_insert("rap_followup")
      ->fields(array(
        'created' => REQUEST_TIME,
        'tid' => $form_state['values']['id'],
        'comment'=> $form_state['values']['comment'],
        //'timeline'=> $form_state['values']['timeline'],
        'followup'=> $form_state['values']['followup'],
      ))
	  
      ->execute();
	//then email the comments to the citizen
	$comment= $form_state['values']['comment'];
    //$timeline= $form_state['values']['timeline'];
    $followup= $form_state['values']['followup'];
	$tid = $form_state['values']['id'];
	
	$update_ticket = db_update("rap_ticket")
      ->fields(array(
        'updated_last' => REQUEST_TIME,
        'closed' => $followup,
      ))
	  ->condition('tid', $tid, '=')
	  ->execute();
	  
	send_followup($comment, $followup);
	//update the citizen page
	$form_state['redirect'] = 'rap/tickettrack/' .$tid;
	//update the issue queue
	
}

function send_followup($c, $f){
	$build = 'sending message';
	return $build;
}
